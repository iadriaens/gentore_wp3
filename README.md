# GenTORE_WP3

This project environment will contain the code of the H2020 GenTORE project (https://www.gentore.eu).

## Context
GenTore aims at developing tools for monitoring and phenotyping of resilience and efficiency. WP3 specifically focuses on on-farm tools to phenotype proxies of resilience & efficiency, based on commercially available precision livestock measures such as milk meters and activity sensors.

### T3.1 -  on-farm tools based on commerically available sensors

## Codes

### Calculation of Lifetime Resilience Score
- **gentore_LRS_calculation.accdb**: Database with example data and weights needed to calculate LRS, as well as queries to do these calculations
- **gentore_LRS_calculation.docx**: Word file with explanation of the calculations

### Sensor features - Type B
- **F1_RankResilience.m**: Calculates the ranking of all animals for which the lifetime performance is available based on their resilience performance
- **F2_SensorFeatures.m**: Calculates sensor features based on daily activity and daily milk production data
- **F3_ActivityFeatures.m**: Calculates sensor features based on activity data only


- **IsoPert.m**: detects milk yield perturbations and puts them in a table together with their characteristics
- **kmo.m**: Kaiser-Meyer-Olkin Measure of Sampling Adequacy- tests a degree of common variance
- **LacFeat2.m**: alternative feature calculations (milk yield)
- **Pattern.m**: detects patterns in a time series


- **S2_PredictFirstLactationSF.m**: prediction of the SF with the models (output = Adriaens et al. 2020)

### Core sensor features - Type A
**F4_CoreSF.m**: as developed by @Wijbrand Ouweltjes


## Data
A sample data set can be requested via ines.adriaens@wur.nl

## Resources
[https://pubmed.ncbi.nlm.nih.gov/32475663/](url)
Adriaens, I., Friggens, N., Ouweltjes, W., Scott, H., Aernouts, B., Statham, J. with Adriaens, I. (corresp. author) (2020). Productive lifespan and resilience rank can be predicted from on-farm first parity sensor time series but not using a common equation across farms. JOURNAL OF DAIRY SCIENCE, 103 (8), 7155-7171. doi: 10.3168/jds.2019-17826
